using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
public class RoadChunk: MonoBehaviour
{
    public Action<Vector2[]> roadChunkGenerated;
    [Range(2, 100)]
    [SerializeField] private int numberOfVertices = 10;
    [SerializeField] private float vertexStepLength = 5f;
    [SerializeField] private float maxHeight = 10f;
    [SerializeField] private float maxDepth = -10f;    

    private EdgeCollider2D edgeCollider;
    private void Awake()
    {
        edgeCollider = GetComponent<EdgeCollider2D>();       
    }
    public void GetMaxHeigt(float value)
    {
        maxHeight = value;
    }
    public void GetMaxDepth(float value)
    {
        maxDepth = value;
    }
    public void Generate()
    {  
        float shift = numberOfVertices * vertexStepLength / 2f;
        Generate(new Vector2(-shift, UnityEngine.Random.Range(maxDepth, maxHeight)));
    }
    public void Generate(Vector2 startPoint)
    {
        Vector2[] points = new Vector2[numberOfVertices];        
        for (int i = 0; i < numberOfVertices; i++)
        {
            if (i == 0)
                points[i] = startPoint;
            else
                points[i] = new Vector2(vertexStepLength * i + startPoint.x, UnityEngine.Random.Range(maxDepth, maxHeight));
        }
        edgeCollider.points = points;

        roadChunkGenerated?.Invoke(points);
    }

    public Vector2 GetFirstPoint()
    {
        return edgeCollider.points.First();        
    }
    public Vector2 GetLastPoint()
    {
        return edgeCollider.points.Last();
    }
}
