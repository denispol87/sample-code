using UnityEngine;

public class CarController : MonoBehaviour
{   
    [SerializeField] private float speed = 1500f;
    [SerializeField] private WheelJoint2D backWheel;        
    private float movement;    
    private float acceleration = 0.05f;

    private bool moveLeft = false;
    private bool moveRight = false;    
    public void MoveLeft(bool value)
    {        
        moveLeft = value;
    }
    public void MoveRight(bool value)
    {
        moveRight = value;
    }
    private void FixedUpdate()
    {              
        if (moveLeft)
            movement = Mathf.Clamp(movement - acceleration, -1, 0);
        if (moveRight)
            movement = Mathf.Clamp01(movement + acceleration);
        if (!moveLeft && !moveRight)
            movement = 0;       

        Move();
    }
    private void Move()
    {
        if (movement == 0f)
            backWheel.useMotor = false;
        else
        {
            backWheel.useMotor = true;
            JointMotor2D motor = new JointMotor2D { motorSpeed = -movement * speed, maxMotorTorque = backWheel.motor.maxMotorTorque };
            backWheel.motor = motor;
        }
    }

}