using UnityEngine;
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance;
    public virtual void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
            Debug.LogError("---Попытка создать повторяющийся Singleton!!!---");
            return;
        }
        else
        {
            Instance = this as T;
        }        
    }   
}