using UnityEngine;

public class CameraController : MonoBehaviour
{        
    private Transform player; 

    private void Start()
    {        
        FindPlayer();
    }
    private void FindPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;     
    }    
    private void FixedUpdate()
    {
        if(player)
        {
            transform.position = new Vector3(player.position.x, player.position.y, -10f);
        }
    }
}
