using System;
using System.Collections;
using UnityEngine;

public static class CoroutineTimer
{
    public static IEnumerator ActionTimer(float time, Action action)
    {
        float duration = time;
        float totalTime = 0;
        while (totalTime <= duration)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        action?.Invoke();
    }
}
