using System;
using TMPro;
using UnityEngine;

public class UIInfo : Singleton<UIInfo>
{
    [SerializeField] private TextMeshProUGUI distanceText;
    [SerializeField] private TextMeshProUGUI wheelieText;
    
    public Action updateUI;
    
    private float timerUiUpdate = 0.5f;
    private void Start()
    {
        distanceText.text = "Distance = 0";
        wheelieText.text = "Wheelie!";
        SetWheelie(false);
        StartCoroutine(CoroutineTimer.ActionTimer(timerUiUpdate, UpdateUI));
    }

    public void SetDistance(float value)
    {
        distanceText.text = "Distance = " + (int)value;
    }
    public void SetWheelie(bool value)
    {
        wheelieText.gameObject.SetActive(value);        
    }    
    private void UpdateUI()
    {
        updateUI?.Invoke();        
        StartCoroutine(CoroutineTimer.ActionTimer(timerUiUpdate, UpdateUI));
    }

}
