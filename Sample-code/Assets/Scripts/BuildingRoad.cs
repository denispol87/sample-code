using System.Linq;
using UnityEngine;

public class BuildingRoad : MonoBehaviour
{
    [SerializeField] private RoadChunk roadChank;
    [SerializeField] private int CountChank = 2;
    [SerializeField] private float maxHeight = 0f;
    [SerializeField] private float maxDepth = -6f;

    private Pool<RoadChunk> poolRoadChunk;
    private RoadChunk LastRoadChank;
    private void Start()
    {
        poolRoadChunk = new Pool<RoadChunk>(Instantiate, transform, roadChank.gameObject);
        for (int i = 0; i < CountChank; i++)
        {
            RoadChunk chank = poolRoadChunk.Get();
            chank.GetMaxHeigt(maxHeight);
            chank.GetMaxDepth(maxDepth);
            if (i == 0)
                chank.Generate();
            else
                chank.Generate(poolRoadChunk.GetActiveObjects()[i-1].GetLastPoint());
        }
        LastRoadChank = poolRoadChunk.GetActiveObjects().Last();
    }
    private void Update()
    {
        if (Camera.main.gameObject.transform.position.x > poolRoadChunk.GetActiveObjects().Last().GetFirstPoint().x)
        {
            RoadChunk firstChank = poolRoadChunk.DelFirst();
            RoadChunk chank = poolRoadChunk.Get();
            chank.Generate(LastRoadChank.GetLastPoint());
            LastRoadChank = chank;
        }
    }
}
