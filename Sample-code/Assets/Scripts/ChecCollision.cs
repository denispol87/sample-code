using UnityEngine;
using UnityEngine.Events;

public class ChecCollision : MonoBehaviour
{
    public UnityEvent<Collider2D, GameObject> onTriggerEnter;
    public UnityEvent<Collider2D, GameObject> onTriggerExit;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        onTriggerEnter?.Invoke(collision, gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        onTriggerExit?.Invoke(collision, gameObject);
    }
}
