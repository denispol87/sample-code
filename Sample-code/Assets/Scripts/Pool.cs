﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class Pool<T> where T : MonoBehaviour
{
    private List<T> hideObjects = new List<T>();
    private List<T> activeObjects = new List<T>();
    private GameObject Prefab;
    private Transform Parent;
    private Func<GameObject, Transform, GameObject> Instantiate;
    public Pool(Func<GameObject,Transform,GameObject> instantiate, Transform parent, GameObject objPrefab)
    {
        Instantiate = instantiate;
        Parent = parent;
        Prefab = objPrefab;
    }
    public void ClearAll()
    {
        for (int i = activeObjects.Count - 1; i >= 0; i--)
        {
            Set(activeObjects[i]);
        } 
    }
    public void Set(T obj)
    {
        obj.gameObject.SetActive(false);
        obj.transform.SetParent(Parent);
        hideObjects.Add(obj);
        activeObjects.Remove(obj);
    }

    public T DelFirst()
    {
        T t = GetActiveObjects()[0];
        Set(GetActiveObjects()[0]);
        return t;
    }

    public T GetInFirst()
    {
        T obj = Get();
        activeObjects.Remove(obj);
        activeObjects.Insert(0, obj);
        obj.transform.SetAsFirstSibling();
        return obj;
    }
    public T Get()
    {
        T obj = null;
        if (hideObjects.Count == 0)
        {
            GameObject gm = Instantiate(Prefab, Parent);
            if (gm.GetComponent<T>() == null)
                obj = gm.AddComponent<T>();
            else
                obj = gm.GetComponent<T>();                        
        }
        else
        {
            obj = hideObjects[0];
            hideObjects.RemoveAt(0);
            obj.transform.SetAsLastSibling();
        }
        obj.gameObject.SetActive(true);
        activeObjects.Add(obj);
        return obj;
    }
    public List<T> GetActiveObjects()
    {
        return activeObjects;
    }
}