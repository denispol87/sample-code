using System;
using System.Collections;
using UnityEngine;

public class CheckWheelie : MonoBehaviour
{
    [SerializeField] private GameObject BackWheel;
    [SerializeField] private GameObject FrontWheel;
    private int FrontWheelCollision = 0;
    private int BackWheelCollision = 0;
    public void TriggerEnter(Collider2D collision, GameObject wheel)
    {
        if (wheel == FrontWheel)
        {
            FrontWheelCollision++;
        }
        if (wheel == BackWheel)
        {
            BackWheelCollision++;
        }
        UIInfo.Instance.SetWheelie(BackWheelCollision > 0 && FrontWheelCollision < 1);
    }
    public void TriggerExit(Collider2D collision, GameObject wheel)
    {
        if (wheel == FrontWheel)
        {
            FrontWheelCollision--;
        }
        if (wheel == BackWheel)
        {
            BackWheelCollision--;
        }
        UIInfo.Instance.SetWheelie(BackWheelCollision > 0 && FrontWheelCollision < 1);
    }   
}
