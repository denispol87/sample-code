using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private void OnEnable()
    {
        UIInfo.Instance.updateUI += InitIndicators;
    }
    private void OnDisable()
    {
        UIInfo.Instance.updateUI -= InitIndicators;
    }
    private void InitIndicators()
    {
        if (transform.position.x < -10)
            Restart();

        UIInfo.Instance.SetDistance(transform.position.x);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {        
        if (collision.CompareTag("Road"))
        {
            Restart();
        }
    }
    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
